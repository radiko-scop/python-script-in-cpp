# https://docs.python.org/3/extending/embedding.html

main: main.cpp
	g++ -c $(shell python3.10-config --cflags) -o main.o main.cpp
	g++ $(shell python3.10-config --ldflags) -lpython3.10 -o main main.o

main_py_file: main_py_file.cpp
	g++ -c $(shell python3.10-config --cflags) -o main_py_file.o main_py_file.cpp
	g++ $(shell python3.10-config --ldflags) -lpython3.10 -o main_py_file main_py_file.o

main_py_file_emb: main_py_file_emb.cpp
	g++ -c $(shell python3.10-config --cflags) -o main_py_file_emb.o main_py_file_emb.cpp
	g++ $(shell python3.10-config --ldflags) -lpython3.10 -o main_py_file_emb main_py_file_emb.o