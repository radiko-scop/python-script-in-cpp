#define PY_SSIZE_T_CLEAN
#include <stdio.h>
#include <Python.h>

static PyObject*
emb_hello(PyObject *self, PyObject *args)
{
    int a;
    int res = PyArg_ParseTuple(args, "i", &a);
    if(!res)
    {
        return NULL;
    }
    printf("hello %d\n", a);
    return PyLong_FromLong(0);
}

static PyMethodDef EmbMethods[] = {
    {"hello", emb_hello, METH_VARARGS,
     "print hello followed by provided integer."},
    {NULL, NULL, 0, NULL}
};

static PyModuleDef EmbModule = {
    PyModuleDef_HEAD_INIT, "emb", NULL, -1, EmbMethods,
    NULL, NULL, NULL, NULL
};

static PyObject*
PyInit_emb(void)
{
    return PyModule_Create(&EmbModule);
}

int
main(int argc, char *argv[])
{
    if (argc < 1) {
        fprintf(stderr,"Usage: call pythonfile\n");
        return 1;
    }

    PyImport_AppendInittab("emb", &PyInit_emb);

    Py_Initialize();

    // Those two lines are not in the original example, but seems necessary to add working dir to path ...
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('.')");

	FILE* fp = fopen(argv[1], "r");
	PyRun_AnyFile(fp, argv[1]);

    // /* Error checking of pName left out */

    if (Py_FinalizeEx() < 0) {
        return 120;
    }
    return 0;
}